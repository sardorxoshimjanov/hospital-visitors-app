package uz.pdp.bmi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.bmi.model.Hospital;
import uz.pdp.bmi.projection.CustomHospital;

import java.util.List;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital,Integer> {
@Query(nativeQuery = true,value = "select h.name as hospitalName,\n" +
        "       h.director as director,\n" +
        "       h.description as description,\n" +
        "       r.name || ', '|| d.name || ', '|| h.address as address,\n" +
        "       (select count(*) as son from doctors join hospitals h on h.id = doctors.hospital_id where h.id = 1) as countSpecialist,\n" +
        "       h.phone_number as phoneNumber,\n" +
        "       h.email as email\n" +
        "from hospitals h\n" +
        "         join districts d on d.id = h.district_id\n" +
        "         join regions r on d.region_id = r.id\n" +
        "where h.id =:hospitalId")
List<CustomHospital> getAllByHospitalById(Integer hospitalId);

@Query(nativeQuery = true,value = "select h.name as hospitalName,\n" +
        "       h.director as director,\n" +
        "       h.description as description,\n" +
        "       r.name || ', '|| d.name || ', '|| h.address as address,\n" +
        "       (select count(*) as son from doctors join hospitals h on h.id = doctors.hospital_id where h.id = 1) as countSpecialist,\n" +
        "       h.phone_number as phoneNumber,\n" +
        "       h.email as email\n" +
        "from hospitals h\n" +
        "         join districts d on d.id = h.district_id\n" +
        "         join regions r on d.region_id = r.id\n" +
        "where h.district_id =:districtId and lower(h.name) like lower(concat('%',:search,'%'))")
Page<CustomHospital> getAllByHospitalSearchDistrictId(Integer districtId,Pageable pageable,String search);
}
