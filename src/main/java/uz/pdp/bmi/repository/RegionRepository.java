package uz.pdp.bmi.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bmi.model.Region;

public interface RegionRepository extends JpaRepository<Region,Integer> {
}
