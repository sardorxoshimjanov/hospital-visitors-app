package uz.pdp.bmi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import uz.pdp.bmi.model.Doctor;
import uz.pdp.bmi.projection.CustomDoctor;
import uz.pdp.bmi.projection.GetAllDoctorsByDistrictId;

import java.util.List;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor,Integer> {
    @Query(nativeQuery = true,value = "select doctors.first_name as firstName,\n" +
            "                   doctors.last_name as lastName,\n" +
            "                   doctors.father_name as fatherName,\n" +
            "                   p.name as positionName,\n" +
            "                   h.name as hospitalName,\n" +
            "                   h.address as hospitalAddress,\n" +
            "                   p2.name as professionName\n" +
            "            from doctors\n" +
            "                   join positions p on p.id = doctors.position_id\n" +
            "                   join professions p2 on doctors.profession_id = p2.id\n" +
            "                   join hospitals h on h.id = doctors.hospital_id\n" +
            "                   where doctors.district_id=:districtId and lower(doctors.first_name) like lower(concat('%',:search,'%'))")
    Page<GetAllDoctorsByDistrictId> getAllByDistrictId(Integer districtId, Pageable pageable,String search);

    @Query(nativeQuery = true,value = "select doctors.first_name as firstName,\n" +
            "                   doctors.last_name as lastName,\n" +
            "                   doctors.father_name as fatherName,\n" +
            "                   p.name as positionName,\n" +
            "                   h.name as hospitalName,\n" +
            "                   h.address as hospitalAddress,\n" +
            "                   p2.name as professionName\n" +
            "            from doctors\n" +
            "                   join positions p on p.id = doctors.position_id\n" +
            "                   join professions p2 on doctors.profession_id = p2.id\n" +
            "                   join hospitals h on h.id = doctors.hospital_id\n" +
            "                   where h.id=:regionId and lower(doctors.first_name) like lower(concat('%',:search,'%'))")
    Page<GetAllDoctorsByDistrictId> getAllByRegionId(Integer regionId, Pageable pageable,String search);

    @Query(nativeQuery = true,value = "select  doctors.last_name||' '|| doctors.first_name ||' '|| doctors.father_name as fullName,\n" +
            "       p2.name as professionName,\n" +
            "       p.name as positionName,\n" +
            "       h.name as hospitalName,\n" +
            "       doctors.description as description\n" +
            "from doctors\n" +
            "join hospitals h on h.id = doctors.hospital_id\n" +
            "join positions p on p.id = doctors.position_id\n" +
            "join professions p2 on doctors.profession_id = p2.id\n" +
            "where doctors.id=:doctorId")
    List<CustomDoctor >doctorById(Integer doctorId);


}
