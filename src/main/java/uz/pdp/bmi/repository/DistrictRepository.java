package uz.pdp.bmi.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bmi.model.District;
import uz.pdp.bmi.projection.CustomDistrict;

import java.util.List;


public interface DistrictRepository extends JpaRepository<District,Integer> {
    @Query(nativeQuery = true,value = "select " +
            "districts.name AS districtName," +
            "r.name AS regionName\n" +
            "from districts join regions r on r.id = districts.region_id\n" +
            "where districts.id =:districtId" )
    List<CustomDistrict> getDistrictById(Integer districtId);

}
