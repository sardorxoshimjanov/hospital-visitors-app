package uz.pdp.bmi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.bmi.model.Profession;

@Repository
public interface ProfessionRepository extends JpaRepository<Profession,Integer> {
}
