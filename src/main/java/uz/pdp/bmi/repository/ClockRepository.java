package uz.pdp.bmi.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.bmi.model.Clock;

import java.util.List;

public interface ClockRepository extends JpaRepository<Clock,Integer> {

    @Query(nativeQuery = true,value = "select * from clocks order by local_time ASC")
    List<Clock> getAllClocks();
}
