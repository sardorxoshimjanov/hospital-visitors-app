package uz.pdp.bmi.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bmi.model.Review;

public interface ReviewRepository extends JpaRepository<Review,Integer> {

}
