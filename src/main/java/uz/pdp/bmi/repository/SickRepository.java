package uz.pdp.bmi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.bmi.model.Sick;


public interface    SickRepository extends JpaRepository<Sick, Integer> {

}
