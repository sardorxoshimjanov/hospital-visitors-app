package uz.pdp.bmi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.bmi.model.District;

import javax.persistence.ManyToOne;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HospitalDto {
    Integer id;
    String name;
    String address;
    String description;
    String email;
    String phoneNumber;
    String director;
    Integer districtId;
}