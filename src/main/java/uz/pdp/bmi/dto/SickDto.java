package uz.pdp.bmi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.bmi.model.District;

import javax.persistence.ManyToOne;
import java.time.LocalDate;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SickDto {
    Integer id;
    String firstName;
    String lastName;
    String fatherName;
    String passport;
    String phoneNumber;
    String password;
    String email;
    String address;
    LocalDate birthday;
    Integer districtId;

    public SickDto(String firstName, String lastName, String fatherName, String passport, String phoneNumber, String password, String email, String address, LocalDate birthday, Integer districtId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.passport = passport;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.email = email;
        this.address = address;
        this.birthday = birthday;
        this.districtId = districtId;
    }
}
