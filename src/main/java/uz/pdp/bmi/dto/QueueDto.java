package uz.pdp.bmi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QueueDto {
    Integer doctorId;
    LocalDate localDate;
    Integer sickId;
    Integer clockId;
}