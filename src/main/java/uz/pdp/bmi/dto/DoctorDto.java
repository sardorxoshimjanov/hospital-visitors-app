package uz.pdp.bmi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DoctorDto {

    String firstName;
    String lastName;
    String fatherName;
    String description;
    LocalDate birthday;
    String phoneNumber;
    String passport;
    String address;
    String email;
    Integer hospitalId;
    Integer positionId;
    Integer professionId;
    Integer districtId;
}
