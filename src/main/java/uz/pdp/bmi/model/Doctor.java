package uz.pdp.bmi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "doctors")
@PackagePrivate
@Builder
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String firstName;
    String lastName;
    String fatherName;
    String description;
    LocalDate birthday;
    String phoneNumber;
    String passport;
    String address;
    String email;

    @ManyToOne(cascade = CascadeType.MERGE)
    Hospital hospital;

    @ManyToOne()
    Position position;

    @OneToOne()
    Attachment attachment;

    @ManyToOne()
    Profession profession;

    @ManyToOne()
    District district;

    public Doctor(String firstName, String lastName, String fatherName, String description, LocalDate birthday, String phoneNumber, String passport, String address, String email, Hospital hospital, Position position, Profession profession, District district) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.description = description;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.passport = passport;
        this.address = address;
        this.email = email;
        this.hospital = hospital;
        this.position = position;
        this.profession = profession;
        this.district = district;
    }

    public Doctor(String firstName, String lastName, String fatherName, String phoneNumber, String passport, String address, String email, Hospital hospital, Position position, District district) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.phoneNumber = phoneNumber;
        this.passport = passport;
        this.address = address;
        this.email = email;
        this.hospital = hospital;
        this.position = position;
        this.district = district;
    }
}


