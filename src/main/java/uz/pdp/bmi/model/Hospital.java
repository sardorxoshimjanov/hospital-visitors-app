package uz.pdp.bmi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity(name = "hospitals")
public class Hospital {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String address;
    String description;
    String email;
    String phoneNumber;
    String director;
    @ManyToOne
    District district;

    public Hospital(String name, String address, String description, String email, String phoneNumber, String director, District district) {
        this.name = name;
        this.address = address;
        this.description = description;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.director = director;
        this.district = district;
    }
}
