package uz.pdp.bmi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity(name = "sicks")
@PackagePrivate
public class Sick {
    @Id
    @GeneratedValue
    Integer id;
    String firstName;
    String lastName;
    String fatherName;
    String passport;
    String phoneNumber;
    String password;
    String email;
    String address;
    LocalDate birthday;
    @ManyToOne
    District district;

    public Sick(String firstName, String lastName, String fatherName, String passport, String phoneNumber, String password, String email, String address, LocalDate birthday, District district) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.passport = passport;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.email = email;
        this.address = address;
        this.birthday = birthday;
        this.district = district;
    }
}
