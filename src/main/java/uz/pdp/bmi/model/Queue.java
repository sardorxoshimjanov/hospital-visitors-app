package uz.pdp.bmi.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@PackagePrivate
@Entity(name = "queues")
public class Queue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @ManyToOne
    Doctor doctor;
    LocalDate localDate;
    @ManyToOne
    Sick sick;


    @ManyToOne
    Clock clock;

    public Queue(Doctor doctor, LocalDate localDate, Sick sick, Clock clock) {
        this.doctor = doctor;
        this.localDate = localDate;
        this.sick = sick;
        this.clock = clock;
    }
}
