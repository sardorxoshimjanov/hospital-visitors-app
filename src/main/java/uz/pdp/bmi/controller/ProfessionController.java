package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.model.Profession;
import uz.pdp.bmi.service.ProfessionService;

@RestController
@AllArgsConstructor
@RequestMapping("/api/profession")
public class ProfessionController {
    private final ProfessionService professionService;

    @PostMapping
    public ResponseEntity<?> addProfession(@RequestBody Profession profession) {
        return professionService.addProfession(profession);
    }

    @DeleteMapping("/{professionId}")
    public ResponseEntity<?> deleteProfession(@PathVariable Integer professionId){
        professionService.deleteProfession(professionId);
        return ResponseEntity.ok("successfully deleted");
    }

    @GetMapping("/{professionId}")
    public ResponseEntity<?> getProfession(@PathVariable Integer professionId){
        ResponseEntity<?> position = professionService.getProfession(professionId);
        return new ResponseEntity<>( position,HttpStatus.ACCEPTED);
    }

    @GetMapping
    public ResponseEntity<?> getAllProfessions(){
        ResponseEntity<?> allProfessions = professionService.getAllProfessions();
        return new ResponseEntity<>( allProfessions,HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateProfession(@PathVariable Integer id,@RequestBody Profession profession){
        return professionService.updateProfession(id,profession);
    }
}
