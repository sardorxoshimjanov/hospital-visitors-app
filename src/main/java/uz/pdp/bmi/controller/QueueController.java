package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.bmi.dto.QueueDto;
import uz.pdp.bmi.service.QueueService;

@RestController
@RequestMapping("/api/queue")
@AllArgsConstructor
public class QueueController {
    private final QueueService queueService;

    @PostMapping
    public ResponseEntity<?> addQueue(@RequestBody QueueDto queueDto) {
        return queueService.addQueue(queueDto);
    }
}
