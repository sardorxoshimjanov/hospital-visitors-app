package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.dto.HospitalDto;
import uz.pdp.bmi.service.HospitalService;

@RestController
@AllArgsConstructor
@RequestMapping("/api/hospital")
public class HospitalController {
    private final HospitalService hospitalService;

    @GetMapping("/district/{districtId}")
    public ResponseEntity<?> getAllDoctorsByDistrictId(@PathVariable Integer districtId,
                                                       @RequestParam(name = "size", defaultValue = "5") int size,
                                                       @RequestParam(name = "page", defaultValue = "1") int page,
                                                       @RequestParam(name = "search", defaultValue = "") String search
    ) {
        ResponseEntity<?> allDoctorsByDistrictId = hospitalService.getAllHospitalBySearchAndBYId(districtId, page, size, search);
        return ResponseEntity.ok(allDoctorsByDistrictId);
    }





    @PostMapping
    public ResponseEntity<?> addHospital(@RequestBody HospitalDto hospital){
        ResponseEntity<?> responseEntity = hospitalService.addHospital(hospital);
        return new ResponseEntity<>(responseEntity, HttpStatus.ACCEPTED);
    }
    @GetMapping("/{hospitalId}")
    public ResponseEntity<?> getHospitalById(@PathVariable Integer hospitalId){
        return hospitalService.getHospitalById(hospitalId);
    }
//    @DeleteMapping("/{districtId}")
//    public ResponseEntity<?> deleteDistrict(@PathVariable Integer districtId){
//        hospitalService.deleteDistrict(districtId);
//        return new ResponseEntity<>( HttpStatus.ACCEPTED);
//    }
//    @GetMapping("/{districtId}")
//    public ResponseEntity<?> getDistrict(@PathVariable Integer districtId){
//        Optional<District> optionalDistrict = hospitalService.findById(districtId);
//        //        ResponseEntity<?> district = districtService.getDistrict(districtId);
////        return new ResponseEntity<>( district,HttpStatus.ACCEPTED);
//        if (!optionalDistrict.isPresent()) {
//            return new ResponseEntity<>("noo",HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>( optionalDistrict,HttpStatus.ACCEPTED);
//    }
//    @GetMapping
//    public ResponseEntity<?> getAllDistricts(){
//        ResponseEntity<?> allDistricts = hospitalService.getAllDistricts();
//        return new ResponseEntity<>( allDistricts,HttpStatus.ACCEPTED);
//    }
}
