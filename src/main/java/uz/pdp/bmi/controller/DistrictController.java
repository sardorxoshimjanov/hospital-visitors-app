package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.model.District;
import uz.pdp.bmi.repository.DistrictRepository;
import uz.pdp.bmi.service.DistrictService;

import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/api/district")
public class DistrictController {
    private final DistrictService districtService;
    private final DistrictRepository districtRepository;

    @DeleteMapping("/{districtId}")
    public ResponseEntity<?> deleteDistrict(@PathVariable Integer districtId){
        districtService.deleteDistrict(districtId);
        return new ResponseEntity<>( HttpStatus.ACCEPTED);
    }
    @GetMapping("/{districtId}")
    public ResponseEntity<?> getDistrict(@PathVariable Integer districtId){
        ResponseEntity<?> district = districtService.getDistrict(districtId);
        return new ResponseEntity<>( district,HttpStatus.ACCEPTED);
    }
    @GetMapping
    public ResponseEntity<?> getAllDistricts(){
        ResponseEntity<?> allDistricts = districtService.getAllDistricts();
        return new ResponseEntity<>( allDistricts.getBody(),HttpStatus.ACCEPTED);
    }

    @GetMapping("/existsById/{districtId}")
    public boolean isDistrictExist(@PathVariable Integer districtId){
        Optional<District> optionalDistrict = districtRepository.findById(districtId);
        return optionalDistrict.isPresent();
    }
}
