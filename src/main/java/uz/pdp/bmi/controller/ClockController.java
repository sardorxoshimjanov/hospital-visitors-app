package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.dto.ClockDto;
import uz.pdp.bmi.service.ClockService;

import java.time.LocalTime;

@RestController
@RequestMapping("/api/clock")
@AllArgsConstructor
public class ClockController {
    private final ClockService clockService;

    @PostMapping
    public ResponseEntity<?> addClock(@RequestBody ClockDto clockDto) {
        return clockService.addClock(clockDto);
    }

    @GetMapping
    public ResponseEntity<?> getAllClocks() {
        return clockService.getAllClocks();
    }

    @GetMapping("/{clockId}")
    public ResponseEntity<?> getClock(@PathVariable Integer clockId) {
        return clockService.getClock(clockId);
    }

    @DeleteMapping("/{clockId}")
    public ResponseEntity<?> deleteClock(@PathVariable Integer clockId) {
        return clockService.deleteClock(clockId);
    }

    @PutMapping("/{clockId}")
    public ResponseEntity<?> updateClock(@PathVariable Integer clockId, @RequestBody ClockDto clockDto) {
        return clockService.updateClock(clockId,clockDto);
    }
}
