package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.model.Region;
import uz.pdp.bmi.service.RegionService;

@RestController
@AllArgsConstructor
@RequestMapping("/api/region")
public class RegionController {
    private final RegionService regionService;
    @PostMapping
    public ResponseEntity<?> addRegion(@RequestBody Region region){
        return regionService.addRegion(region);

    }
    @DeleteMapping("/{districtId}")
    public ResponseEntity<?> deleteRegion(@PathVariable Integer districtId){
        regionService.deleteRegion(districtId);
        return new ResponseEntity<>( HttpStatus.ACCEPTED);
    }
    @GetMapping("/{districtId}")
    public ResponseEntity<?> getRegion(@PathVariable Integer districtId){
        ResponseEntity<?> district = regionService.getRegion(districtId);
        return new ResponseEntity<>( district,HttpStatus.ACCEPTED);
    }
    @GetMapping
    public ResponseEntity<?> getAllRegions(){
        ResponseEntity<?> allRegions = regionService.getAllRegions();
        return new ResponseEntity<>( allRegions,HttpStatus.ACCEPTED);
    }

    @PutMapping ("/{id}")
    public ResponseEntity<?> updateRegion(@PathVariable Integer id,@RequestBody Region region){
        return regionService.updateRegion(id,region);
    }
}
