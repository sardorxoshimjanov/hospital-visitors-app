package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.dto.SickDto;
import uz.pdp.bmi.model.Sick;
import uz.pdp.bmi.repository.SickRepository;
import uz.pdp.bmi.service.SickService;


import java.util.Optional;

@RestController
@RequestMapping("/api/sick")
@AllArgsConstructor
public class SickController {
    private final SickService sickService;
    private final SickRepository sickRepository;

    @GetMapping("/{sickId}")
    public ResponseEntity<?> getSick(@PathVariable Integer sickId) {
        ResponseEntity<?> sick = sickService.getSick(sickId);
        return new ResponseEntity<>(sick, HttpStatus.ACCEPTED);
    }
    @PostMapping
    public ResponseEntity<?> addSick(@RequestBody SickDto sickDto){
        return sickService.addSick(sickDto);
    }
    @GetMapping
    public ResponseEntity<?> getAllSicks(){
        return sickService.getAllSicks();
    }
    @DeleteMapping("/{sickId}")
    public ResponseEntity<?> deleteSick(@PathVariable Integer sickId){
        return sickService.deleteSick(sickId);
    }

    @GetMapping("/existsById/{sickId}")
    public boolean isSickExist(@PathVariable Integer sickId){
        Optional<Sick> optionalSick = sickRepository.findById(sickId);
        return optionalSick.isPresent();
    }

}
