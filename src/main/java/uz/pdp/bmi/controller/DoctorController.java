package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.bmi.dto.DoctorDto;
import uz.pdp.bmi.model.Doctor;
import uz.pdp.bmi.repository.DoctorRepository;
import uz.pdp.bmi.service.DoctorService;


import javax.persistence.criteria.CriteriaBuilder;
import java.io.IOException;
import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/api/doctor")
public class DoctorController {
    private final DoctorService doctorService;
    private final DoctorRepository doctorRepository;


    @GetMapping("/district/{districtId}")
    public ResponseEntity<?> getAllDoctorsByDistrictId(@PathVariable Integer districtId,
                                                       @RequestParam(name = "size", defaultValue = "5") int size,
                                                       @RequestParam(name = "page", defaultValue = "1") int page,
                                                       @RequestParam(name = "search", defaultValue = "") String search
    ) {

        ResponseEntity<?> allDoctorsByDistrictId = doctorService.getAllDoctorsByDistrictId(districtId, page, size, search);
        return ResponseEntity.ok(allDoctorsByDistrictId);
    }


    @PostMapping
    public ResponseEntity<?> addDoctor(@RequestBody DoctorDto doctorDto, @RequestBody MultipartFile file) {
        try {
            ResponseEntity<?> responseEntity = doctorService.addDoctor(doctorDto, file);
            return new ResponseEntity<>(responseEntity, HttpStatus.ACCEPTED);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/existsById/{doctorId}")
    public boolean isDoctorExist(@PathVariable Integer doctorId) {
        Optional<Doctor> optionalDoctor = doctorRepository.findById(doctorId);
        return optionalDoctor.isPresent();
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getDoctorById(@PathVariable Integer id) {
        return doctorService.getDoctorById(id);
    }


//    @DeleteMapping("/{districtId}")
//    public ResponseEntity<?> deleteDistrict(@PathVariable Integer districtId){
//        hospitalService.deleteDistrict(districtId);
//        return new ResponseEntity<>( HttpStatus.ACCEPTED);
//    }
//    @GetMapping("/{districtId}")
//    public ResponseEntity<?> getDistrict(@PathVariable Integer districtId){
//        Optional<District> optionalDistrict = hospitalService.findById(districtId);
//        //        ResponseEntity<?> district = districtService.getDistrict(districtId);
////        return new ResponseEntity<>( district,HttpStatus.ACCEPTED);
//        if (!optionalDistrict.isPresent()) {
//            return new ResponseEntity<>("noo",HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity<>( optionalDistrict,HttpStatus.ACCEPTED);
//    }
//    @GetMapping
//    public ResponseEntity<?> getAllDistricts(){
//        ResponseEntity<?> allDistricts = hospitalService.getAllDistricts();
//        return new ResponseEntity<>( allDistricts,HttpStatus.ACCEPTED);
//    }
}
