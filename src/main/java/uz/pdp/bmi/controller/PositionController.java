package uz.pdp.bmi.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.bmi.service.PositionService;

@RestController
@AllArgsConstructor
@RequestMapping("/api/position")
public class PositionController {
    private final PositionService positionService;

    @PostMapping
    public ResponseEntity<?> addPosition(@RequestBody String name) {
        ResponseEntity<?> responseEntity = positionService.addPosition(name);
        return new ResponseEntity<>(responseEntity, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{positionId}")
    public ResponseEntity<?> deletePosition(@PathVariable Integer positionId){
        positionService.deletePosition(positionId);
        return ResponseEntity.ok("successfully deleted");
    }
    @GetMapping("/{positionId}")
    public ResponseEntity<?> getPosition(@PathVariable Integer positionId){
        ResponseEntity<?> position = positionService.getPosition(positionId);
        return new ResponseEntity<>( position,HttpStatus.ACCEPTED);
    }
    @GetMapping
    public ResponseEntity<?> getAllPositions(){
        ResponseEntity<?> allPositions = positionService.getAllPositions();
        return new ResponseEntity<>( allPositions,HttpStatus.ACCEPTED);
    }

}
