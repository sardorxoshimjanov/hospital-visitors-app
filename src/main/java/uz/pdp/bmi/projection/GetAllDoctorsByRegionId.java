package uz.pdp.bmi.projection;

public interface GetAllDoctorsByRegionId {
   String getFirstName();
   String getLastName();
   String getFatherName();
   String getPositionName();
   String getHospitalName();
   String getHospitalAddress();
   String getProfessionName();
   String getRegionName();
   String getDistrictName();
}
