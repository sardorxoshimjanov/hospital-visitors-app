package uz.pdp.bmi.projection;

public interface GetAllDoctorsByDistrictId {
   String getFirstName();
   String getLastName();
   String getFatherName();
   String getPositionName();
   String getHospitalName();
   String getHospitalAddress();
   String getProfessionName();
   String getRegionName();
   String getDistrictName();
}
