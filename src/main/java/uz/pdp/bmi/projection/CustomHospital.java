package uz.pdp.bmi.projection;

public interface CustomHospital {
    String getHospitalName();
    String getAddress();
    String getDirector();
    String getPhoneNumber();
    String getEmail();
    String getCountSpecialist();
    String getDescription();

}
