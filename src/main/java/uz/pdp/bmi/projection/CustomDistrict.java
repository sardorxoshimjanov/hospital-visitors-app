package uz.pdp.bmi.projection;

public interface CustomDistrict {
    String getDistrictName();
    String getRegionName();
}
