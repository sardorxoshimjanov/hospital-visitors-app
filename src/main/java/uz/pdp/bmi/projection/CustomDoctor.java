package uz.pdp.bmi.projection;

public interface CustomDoctor {
      String  getFullName();
      String  getProfessionName();
      String  getPositionName();
      String  getHospitalName();
      String  getDescription();

}
