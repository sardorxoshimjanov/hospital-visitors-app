package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.model.District;
import uz.pdp.bmi.model.Region;
import uz.pdp.bmi.projection.CustomDistrict;
import uz.pdp.bmi.repository.DistrictRepository;
import uz.pdp.bmi.repository.RegionRepository;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class DistrictService {
    private final DistrictRepository districtRepository;
    private final RegionRepository regionRepository;

    public ResponseEntity<?> getDistrict(Integer districtId){
        List<CustomDistrict> districtById = districtRepository.getDistrictById(districtId);
        return ResponseEntity.ok(districtById);
    }
    public ResponseEntity<?> getAllDistricts(){
        List<District> districtList = districtRepository.findAll();
        return ResponseEntity.ok(districtList);
    }
    public boolean isDistrictExist(Integer districtId){
        Optional<District> optionalDistrict = districtRepository.findById(districtId);
        return optionalDistrict.isPresent();
    }

//    public ResponseEntity<?> addDistrict(String districtName,Integer regionId){
//        Optional<Region> optionalRegion = regionRepository.findById(regionId);
//        District build = District.builder()
//                .name(districtName)
//                .region(optionalRegion.get())
//                .build();
//
//        District district = districtRepository.save(build);
//        return ResponseEntity.ok(district);
//    }
    public ResponseEntity<?> deleteDistrict(Integer districtId){
        regionRepository.deleteById(districtId);
        return ResponseEntity.ok("successfully delete");
    }
}
