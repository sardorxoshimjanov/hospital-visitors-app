package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.model.Position;
import uz.pdp.bmi.repository.PositionRepository;


import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PositionService {
    private final PositionRepository positionRepository;

    public ResponseEntity<?> getAllPositions() {
        List<Position> positionList = positionRepository.findAll();
        return ResponseEntity.ok(positionList);
    }
    public ResponseEntity<?> getPosition(Integer id) {
        Optional<Position> optionalPosition = positionRepository.findById(id);
        return ResponseEntity.ok(optionalPosition);
    }


    public ResponseEntity<?> addPosition(String positionName) {
        Position position = new Position();
        position.setName(positionName);
        Position save = positionRepository.save(position);
        return new ResponseEntity<>(save, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> deletePosition(Integer positionId) {
        positionRepository.deleteById(positionId);
        return ResponseEntity.ok("Successfully deleted");
    }
}
