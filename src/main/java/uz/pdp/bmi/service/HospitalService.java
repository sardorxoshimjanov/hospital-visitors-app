package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.dto.HospitalDto;
import uz.pdp.bmi.model.District;
import uz.pdp.bmi.model.Hospital;
import uz.pdp.bmi.projection.CustomHospital;
import uz.pdp.bmi.repository.DistrictRepository;
import uz.pdp.bmi.repository.HospitalRepository;

import java.util.List;
import java.util.Optional;

import static org.apache.logging.log4j.ThreadContext.isEmpty;

@Service
@AllArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final DistrictRepository districtRepository;
    public ResponseEntity<?> getAllHospitals(){
        List<Hospital> hospitals = hospitalRepository.findAll();
        return ResponseEntity.ok(hospitals);
    }

    public ResponseEntity<?> getAllHospitalBySearchAndBYId(Integer districtId, Integer page, Integer size, String search){
        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );
        Page<CustomHospital> byHospitalSearch = hospitalRepository.getAllByHospitalSearchDistrictId(districtId, pageable,search);
        if (byHospitalSearch.isEmpty())
            return new ResponseEntity<>("Not Found Hospital",HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(byHospitalSearch,HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> getHospitalById(Integer hospitalId){
        List<CustomHospital> customHospitals = hospitalRepository.getAllByHospitalById(hospitalId);
        if (customHospitals.isEmpty())
            return new ResponseEntity<>("Not found Hospital",HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(customHospitals,HttpStatus.ACCEPTED);
    }
    public ResponseEntity<?> deleteHospital(Integer hospitalId){
        hospitalRepository.deleteById(hospitalId);
        return ResponseEntity.ok("successfully deleted");
    }
    public ResponseEntity<?> addHospital(HospitalDto hospitalDto){

        Optional<District> optionalDistrict = districtRepository.findById(hospitalDto.getDistrictId());

        Hospital buildHospital = Hospital.builder()
                .address(hospitalDto.getAddress())
                .description(hospitalDto.getAddress())
                .director(hospitalDto.getDirector())
                .district(optionalDistrict.get())
                .email(hospitalDto.getEmail())
                .name(hospitalDto.getName())
                .phoneNumber(hospitalDto.getPhoneNumber())
                .build();
        Hospital save = hospitalRepository.save(buildHospital);
        return new ResponseEntity<>(save, HttpStatus.ACCEPTED);
    }

    public boolean isHospitalExist(Integer hospitalId){
        Optional<Hospital> byId = hospitalRepository.findById(hospitalId);
        return byId.isPresent();
    }
}
