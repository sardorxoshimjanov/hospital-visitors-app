package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.model.Region;
import uz.pdp.bmi.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    public ResponseEntity<?> getRegion(Integer regionId){
        Optional<Region> optionalRegion = regionRepository.findById(regionId);
        return ResponseEntity.ok(optionalRegion);
    }
    public ResponseEntity<?> getAllRegions(){
        List<Region> optionalRegion = regionRepository.findAll();
        return ResponseEntity.ok(optionalRegion);
    }

    public ResponseEntity<?> addRegion(Region region){
        Region build = Region
                .builder()
                .name(region.getName())
                .build();
        regionRepository.save(build);
        return ResponseEntity.ok(build);
    }
    public void deleteRegion(Integer regionId){
        regionRepository.deleteById(regionId);
        ResponseEntity.ok("successfully delete");
    }

    public ResponseEntity<?> updateRegion(Integer id, Region region) {

        Optional<Region> optional = regionRepository.findById(id);
        if (optional.isEmpty()) {
            return ResponseEntity.ok("Region not found");
        }
        Region newRegion = new Region(region.getName());
        regionRepository.save(newRegion);
        return ResponseEntity.ok("Successfully updated");
    }
}
