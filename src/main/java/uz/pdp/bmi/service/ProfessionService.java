package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.model.Profession;
import uz.pdp.bmi.repository.ProfessionRepository;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProfessionService {
    private final ProfessionRepository professionRepository;

    public ResponseEntity<?> getAllProfessions() {
        List<Profession> repositoryList = professionRepository.findAll();
        return  ResponseEntity.ok(repositoryList);
    }

    public ResponseEntity<?> getProfession(Integer id) {
        Optional<Profession> optionalProfession = professionRepository.findById(id);
        return  ResponseEntity.ok(optionalProfession);
    }

    public ResponseEntity<?> addProfession(Profession profession) {
        Profession newProfession = new Profession();
        newProfession.setName(profession.getName());
        professionRepository.save(newProfession);
        return ResponseEntity.ok("Successfully added");
    }

    public void deleteProfession(Integer positionId) {
        professionRepository.deleteById(positionId);
        ResponseEntity.ok("Successfully deleted");
    }

    public ResponseEntity<?> updateProfession(Integer positionId,Profession profession) {
        Optional<Profession> newProfession = professionRepository.findById(positionId);
        if (newProfession.isEmpty()) return null;
        newProfession.get().setName(profession.getName());
        professionRepository.save(newProfession.get());
        return ResponseEntity.ok("Successfully updated");
    }
}
