package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.dto.QueueDto;
import uz.pdp.bmi.model.Clock;
import uz.pdp.bmi.model.Doctor;
import uz.pdp.bmi.model.Queue;
import uz.pdp.bmi.model.Sick;
import uz.pdp.bmi.repository.ClockRepository;
import uz.pdp.bmi.repository.DoctorRepository;
import uz.pdp.bmi.repository.QueueRepository;
import uz.pdp.bmi.repository.SickRepository;


import java.util.Optional;


@Service
@AllArgsConstructor
public class QueueService {

    private final QueueRepository queueRepository;

    private final DoctorRepository doctorRepository;

    private final SickRepository sickRepository;
    private final ClockRepository clockRepository;

    public ResponseEntity<?> addQueue(QueueDto queueDto) {
        Integer clockId = queueDto.getClockId();

        Optional<Doctor> optionalDoctor = doctorRepository.findById(queueDto.getDoctorId());
        if (optionalDoctor.isEmpty()) return new ResponseEntity<>("not found doctor",HttpStatus.BAD_REQUEST);

        Optional<Sick> optionalSick = sickRepository.findById(queueDto.getSickId());
        if (optionalSick.isEmpty()) return new ResponseEntity<>("not found sick",HttpStatus.BAD_REQUEST);

        Optional<Clock> optionalClock = clockRepository.findById(clockId);
        if (optionalClock.isEmpty()) return new ResponseEntity<>("Clock not found",HttpStatus.BAD_REQUEST);

        Queue build = Queue.builder()
                .clock(optionalClock.get())
                .doctor(optionalDoctor.get())
                .sick(optionalSick.get())
                .localDate(queueDto.getLocalDate())
                .build();
        Queue save = queueRepository.save(build);
        return new ResponseEntity<>(save, HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> getQueueByDoctorId(Integer doctorId) {
        return null;
    }


}
