package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.bmi.dto.DoctorDto;
import uz.pdp.bmi.model.*;
import uz.pdp.bmi.projection.CustomDoctor;
import uz.pdp.bmi.projection.GetAllDoctorsByDistrictId;
import uz.pdp.bmi.repository.*;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DoctorService {
    private final DistrictRepository districtRepository;
    private final DoctorRepository doctorRepository;
    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;

    private final HospitalService hospitalService;
    private final HospitalRepository hospitalRepository;
    private final PositionRepository positionRepository;
    private final ProfessionRepository professionRepository;


    public ResponseEntity<?> addDoctor(DoctorDto doctorDto, MultipartFile attachment) throws IOException {

        Attachment newAttachment = new Attachment();
        newAttachment.setContentType(attachment.getContentType());
        newAttachment.setName(attachment.getName());
        newAttachment.setSize(attachment.getSize());
        Attachment save = attachmentRepository.save(newAttachment);

        AttachmentContent attachmentContent = new AttachmentContent();
        attachmentContent.setAttachment(newAttachment);
        attachmentContent.setDate(attachment.getBytes());
        AttachmentContent attachmentContent1 = attachmentContentRepository.save(attachmentContent);

        Optional<District> optionalDistrict = districtRepository.findById(doctorDto.getDistrictId());

        Optional<Hospital> optionalHospital = hospitalRepository.findById(doctorDto.getHospitalId());

        Optional<Position> optionalPosition = positionRepository.findById(doctorDto.getPositionId());
        Optional<Profession> optionalProfession = professionRepository.findById(doctorDto.getProfessionId());
        Doctor build = Doctor.builder()
                .firstName(doctorDto.getFirstName())
                .lastName(doctorDto.getLastName())
                .fatherName(doctorDto.getFatherName())
                .description(doctorDto.getDescription())
                .birthday(doctorDto.getBirthday())
                .phoneNumber(doctorDto.getPhoneNumber())
                .passport(doctorDto.getPassport())
                .address(doctorDto.getAddress())
                .email(doctorDto.getEmail())
                .hospital(optionalHospital.get())
                .position(optionalPosition.get())
                .profession(optionalProfession.get())
                .district(optionalDistrict.get())
                .attachment(save)
                .build();
        Doctor doctor = doctorRepository.save(build);
        return new ResponseEntity<>("successfully added", HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> getAllDoctorsByDistrictId(Integer districtId, Integer page, Integer size, String search) {
        Pageable pageable = PageRequest.of(
                page - 1,
                size
        );

        Page<GetAllDoctorsByDistrictId> allDoctorsByDistrictIdList = doctorRepository.getAllByDistrictId(districtId, pageable, search);
        return ResponseEntity.ok(allDoctorsByDistrictIdList);
    }

    public ResponseEntity<?> getDoctorById(Integer doctorId) {
        List<CustomDoctor> doctorList = doctorRepository.doctorById(doctorId);
        if (doctorList.isEmpty())
            return ResponseEntity.ok("Not found Doctor");
        return new ResponseEntity<>(doctorList,HttpStatus.ACCEPTED);
    }
}
