package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.dto.ClockDto;
import uz.pdp.bmi.model.Clock;
import uz.pdp.bmi.repository.ClockRepository;


import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClockService {
    private final ClockRepository clockRepository;
    public ResponseEntity<?> addClock(ClockDto clockDto){
        Clock clock = new Clock();
        LocalTime time=LocalTime.of(clockDto.getHour(),clockDto.getMinute());
        clock.setLocalTime(time);
        Clock save = clockRepository.save(clock);
        return ResponseEntity.ok(save);
    }

    public ResponseEntity<?> getAllClocks(){
        List<Clock> clockList = clockRepository.getAllClocks();
        return ResponseEntity.ok(clockList);
    }

    public ResponseEntity<?> getClock(Integer clockId){
        Optional<Clock> optionalClock = clockRepository.findById(clockId);
        return ResponseEntity.ok(optionalClock);
    }
    public ResponseEntity<?> deleteClock(Integer clockId){
        clockRepository.deleteById(clockId);
        return ResponseEntity.ok("successfully deleted");
    }
    public ResponseEntity<?> updateClock(Integer clockId, ClockDto clockDto){

        LocalTime time=LocalTime.of(clockDto.getHour(),clockDto.getMinute());
        System.out.println(time);
        Optional<Clock> optionalClock = clockRepository.findById(clockId);
        optionalClock.get().setLocalTime(time);
        System.out.println(optionalClock.get());
        clockRepository.save(optionalClock.get());
        return ResponseEntity.ok("successfully updated");
    }
}
