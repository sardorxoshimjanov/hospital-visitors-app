package uz.pdp.bmi.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.bmi.dto.SickDto;
import uz.pdp.bmi.model.District;
import uz.pdp.bmi.model.Sick;
import uz.pdp.bmi.repository.DistrictRepository;
import uz.pdp.bmi.repository.SickRepository;


import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SickService {
    private final SickRepository sickRepository;
    private final DistrictRepository districtRepository;

//    private final AddressClient addressClient;
    public ResponseEntity<?> getAllSicks(){
        List<Sick> sickList = sickRepository.findAll();
        return new ResponseEntity<>(sickList, HttpStatus.ACCEPTED);
    }
    public ResponseEntity<?> getSick(Integer sick_id){
        Optional<Sick> byId = sickRepository.findById(sick_id);
        return new ResponseEntity<>(byId, HttpStatus.ACCEPTED);
    }
    public ResponseEntity<?> deleteSick(Integer sick_id){
         sickRepository.deleteById(sick_id);
        return  ResponseEntity.ok("successfully deleted");
    }
    public ResponseEntity<?> addSick(SickDto sickDto){

        Optional<District> optionalDistrict = districtRepository.findById(sickDto.getDistrictId());
        Sick build = Sick.builder()
                .address(sickDto.getAddress())
                .birthday(sickDto.getBirthday())
                .fatherName(sickDto.getFatherName())
                .email(sickDto.getEmail())
                .firstName(sickDto.getFirstName())
                .lastName(sickDto.getLastName())
                .district(optionalDistrict.get())
                .passport(sickDto.getPassport())
                .phoneNumber(sickDto.getPhoneNumber())
                .password(sickDto.getPassword())
                .build();
        Sick save = sickRepository.save(build);
        return new ResponseEntity<>(save, HttpStatus.ACCEPTED);
    }

}
