package uz.pdp.bmi.common;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.pdp.bmi.model.*;
import uz.pdp.bmi.repository.*;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.sql.init.mode}")
    String initMode;

    final AttachmentRepository attachmentRepository;
    final AttachmentContentRepository attachmentContentRepository;
    final ClockRepository clockRepository;
    final DistrictRepository districtRepository;
    final DoctorRepository doctorRepository;
    final HospitalRepository hospitalRepository;
    final PositionRepository positionRepository;
    final ProfessionRepository professionRepository;
    final QueueRepository queueRepository;
    final RegionRepository regionRepository;
    final ReviewRepository reviewRepository;
    final SickRepository sickRepository;

    public DataLoader(AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository, ClockRepository clockRepository, DistrictRepository districtRepository, DoctorRepository doctorRepository, HospitalRepository hospitalRepository, PositionRepository positionRepository, ProfessionRepository professionRepository, QueueRepository queueRepository, RegionRepository regionRepository, ReviewRepository reviewRepository, SickRepository sickRepository) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
        this.clockRepository = clockRepository;
        this.districtRepository = districtRepository;
        this.doctorRepository = doctorRepository;
        this.hospitalRepository = hospitalRepository;
        this.positionRepository = positionRepository;
        this.professionRepository = professionRepository;
        this.queueRepository = queueRepository;
        this.regionRepository = regionRepository;
        this.reviewRepository = reviewRepository;
        this.sickRepository = sickRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")){
            Region tashkent=new Region("Tashkent");
            Region namangan=new Region("Namangan");
            Region bukhara=new Region("Bukhara");
            Region andijan=new Region("Andijan");

            Region saveTashkent = regionRepository.save(tashkent);
            Region saveNamangan = regionRepository.save(namangan);
            Region saveBukhara = regionRepository.save(bukhara);
            Region saveAndijan = regionRepository.save(andijan);

            District chartak = new District("Chartak", saveNamangan);
            District chust = new District("Chust", saveNamangan);
            District uychi = new District("Uychi", saveNamangan);
            District kasansay = new District("Kasansay", saveNamangan);

            District shaykhontohur = new District("Shaykhontohur", saveTashkent);
            District olmazor = new District("Olmazor", saveTashkent);
            District mirabod = new District("Mirabod", saveTashkent);
            District uchtepa = new District("Uchtepa", saveTashkent);
            District yunusabad = new District("Yunusabad", saveTashkent);

            District alat = new District("Alat", saveBukhara);
            District  jondor= new District("Jondor", saveBukhara);
            District kagan = new District("Kagan", saveBukhara);
            District peshku = new District("Peshku", saveBukhara);
            District karakul = new District("Karakul", saveBukhara);

            District asaka = new District("Asaka", saveAndijan);
            District baliqchi = new District("Baliqchi", saveAndijan);
            District boz = new District("Boz", saveAndijan);
            District oltinkol = new District("Oltinkol", saveAndijan);
            District ulugnor = new District("Ulugnor", saveAndijan);

            districtRepository.save(alat);
            districtRepository.save(jondor);
            districtRepository.save(kagan);
            districtRepository.save(peshku);
            districtRepository.save(karakul);
            districtRepository.save(baliqchi);
            districtRepository.save(chartak);
            districtRepository.save(oltinkol);
            districtRepository.save(uychi);
            districtRepository.save(asaka);
            districtRepository.save(chust);
            districtRepository.save(kasansay);
            District saveShaykhontohur = districtRepository.save(shaykhontohur);
            districtRepository.save(olmazor);
            District saveMirabod = districtRepository.save(mirabod);
            districtRepository.save(uchtepa);
            districtRepository.save(yunusabad);
            districtRepository.save(ulugnor);
            districtRepository.save(boz);


            Position pediatr = new Position("Pediatr");
            Position jarroh = new Position("Jarroh");
            Position psixiatr = new Position("Psixiatr");
            Position terapevt = new Position("Terapevt");

            Position savePediatr = positionRepository.save(pediatr);
            Position saveJarroh = positionRepository.save(jarroh);
            Position savePsixiatr = positionRepository.save(psixiatr);
            Position saveTerapevt = positionRepository.save(terapevt);


            Hospital hospital = new Hospital("1 SON OILAVIY POLIKLINIKA",
                    "Chexov Ko'chasi, 6",
                    "",
                    "shifoxona@gmail.com",
                    "+998(71)2540481",
                    "Rasulov Ismoil",
                    saveMirabod);
            Hospital hospital1 = new Hospital("11 SON OILAVIY POLIKLINIKA",
                    "Navoiy Ko'chasi, 2",
                    "",
                    "shifoxona1@gmail.com",
                    "+998(71)2540782",
                    "Jamalov Husanboy",
                    saveMirabod);
            Hospital hospital2 = new Hospital("6 SON OILAVIY POLIKLINIKA",
                    "Beruniy Ko'chasi, 12",
                    "",
                    "shifoxona2@gmail.com",
                    "+998(71)2540581",
                    "Eldor Hoshimov",
                    saveMirabod);
            Hospital hospital3 = new Hospital("5 SON OILAVIY POLIKLINIKA",
                    "CHEXOV Ko'chasi, 6",
                    "",
                    "shifoxona3@gmail.com",
                    "+998(71)2540381",
                    "Jahongir Turdiyev",
                    saveMirabod);
            Hospital hospital4 = new Hospital("12 SON OILAVIY POLIKLINIKA",
                    "Amir Temur Ko'chasi 4",
                    "",
                    "shifoxona4@gmail.com",
                    "+998(71)2541481",
                    "Ergashev Muhsin",
                    saveMirabod);

            Hospital saveHospital1 = hospitalRepository.save(hospital);
            Hospital saveHospital2 = hospitalRepository.save(hospital2);
            Hospital saveHospital3 = hospitalRepository.save(hospital3);
            Hospital saveHospital4 = hospitalRepository.save(hospital4);

            Profession profession = new Profession("profession");
            Profession profession1 = new Profession("profession1");
            Profession saveProfession = professionRepository.save(profession);
            Profession saveProfession1  = professionRepository.save(profession1);


            LocalDate date1 = LocalDate.of(1989, 10, 23);
            LocalDate date2 = LocalDate.of(1983, 1, 2);
            LocalDate date3 = LocalDate.of(1969, 5, 15);
            LocalDate date4 = LocalDate.of(1974, 8, 13);
            LocalDate date5 = LocalDate.of(1982, 11, 6);

            Doctor doctor = new Doctor("Abdumalik", "Xusanovich", "Ismoilov", "+99889098765410", "AA7896542", "Farobiy kocha", "abdumalik@gamil.com", saveHospital1, saveJarroh, saveMirabod);
            Doctor doctor1 = new Doctor("Abdumalik", "Xusanovich", "Ismoilov", "",date1 , "+99889098765410", "AA7896542", "Farobiy kocha", "abdumalik@gamil.com", saveHospital1, saveJarroh, saveProfession, saveMirabod);
            Doctor doctor2 = new Doctor("Muhriddin", "Ziyodila ogli", "Hakimov", "",date2 , "+9988909876541", "AA7896543", "Farobiy kocha", "hakimov@gamil.com", saveHospital2, saveJarroh, saveProfession, saveMirabod);
            Doctor doctor3 = new Doctor("Sobir", "Saidovich", "Yaxshiboyev", "",date3 , "+99889098765412", "AA7896541", "Navoiy kocha", "yaxshiboyev@gamil.com", saveHospital3, saveJarroh, saveProfession, saveMirabod);
            Doctor doctor4 = new Doctor("Omina", "Elyor qizi", "Goipova", "",date4 , "+99889098765413", "AA7896545", "Farobiy kocha", "omina@gamil.com", saveHospital1, saveJarroh, saveProfession, saveMirabod);
            Doctor doctor5 = new Doctor("Gulchehara", "Jora qizi", "Jumanova", "",date5 , "+99889098765414", "AA7893542", "Amir Temur kocha", "guli_93@gamil.com", saveHospital1, saveJarroh, saveProfession, saveMirabod);




//          doctorRepository.save(doctor1);
            Doctor saveDoctor = doctorRepository.save(doctor2);
            Doctor saveDoctor1 = doctorRepository.save(doctor3);
            Doctor saveDoctor2 = doctorRepository.save(doctor4);
            doctorRepository.save(doctor5);
            doctorRepository.save(doctor);
            doctorRepository.save(doctor1);

            LocalDate localDate=LocalDate.of(1999,06,11);
            Sick sick = new Sick("Sardor","Xoshimjanov","Jahongir ogli","AA1111111","+998991111111","1234","ass@gmail.com","Olmazor",localDate,saveShaykhontohur);
            Sick sick1 = new Sick("Eldor","Xoshimjanov","Jahongir ogli","AA1111112","+998991111112","1235","as2@gmail.com","Chorsu",localDate,saveMirabod);
            Sick sick2 = new Sick("Muhisin","Xolmatov","Muhiddin ogli","AA1111113","+998991111113","1236","ass1@gmail.com","Qumtepa",localDate,saveShaykhontohur);

            Sick saveSick1 = sickRepository.save(sick);
            Sick saveSick2 = sickRepository.save(sick1);
            Sick saveSick3 = sickRepository.save(sick2);




            Clock clock = new Clock(LocalTime.of(9, 00));
            Clock clock1 = new Clock(LocalTime.of(9, 30));
            Clock clock2 = new Clock(LocalTime.of(10, 00));
            Clock clock3 = new Clock(LocalTime.of(10, 30));
            Clock clock4 = new Clock(LocalTime.of(11, 00));
            Clock clock5 = new Clock(LocalTime.of(12, 00));
            Clock clock6 = new Clock(LocalTime.of(12, 30));
            Clock clock7 = new Clock(LocalTime.of(14, 00));
            Clock clock8 = new Clock(LocalTime.of(14, 30));
            Clock clock9 = new Clock(LocalTime.of(13, 00));
            Clock clock10 = new Clock(LocalTime.of(13, 30));
            Clock clock11 = new Clock(LocalTime.of(14, 00));
            Clock clock12 = new Clock(LocalTime.of(14, 30));
            Clock clock13 = new Clock(LocalTime.of(15, 00));
            Clock clock14 = new Clock(LocalTime.of(15, 30));
            Clock clock15 = new Clock(LocalTime.of(16, 00));
            Clock clock16 = new Clock(LocalTime.of(16, 30));
            Clock clock17 = new Clock(LocalTime.of(17, 30));
            Clock clock18 = new Clock(LocalTime.of(17, 00));
            Clock clock19 = new Clock(LocalTime.of(18, 00));
            Clock clock20 = new Clock(LocalTime.of(18, 30));


            clockRepository.save(clock16);
            clockRepository.save(clock17);
            clockRepository.save(clock18);
            clockRepository.save(clock19);
            Clock clock2000 = clockRepository.save(clock20);
            clockRepository.save(clock8);
            clockRepository.save(clock9);
            Clock clock1000 = clockRepository.save(clock10);
            clockRepository.save(clock11);
            clockRepository.save(clock12);
            clockRepository.save(clock13);
            clockRepository.save(clock14);
            clockRepository.save(clock15);
            clockRepository.save(clock);
            clockRepository.save(clock1);
            clockRepository.save(clock2);
            clockRepository.save(clock3);
            clockRepository.save(clock4);
            clockRepository.save(clock5);
            clockRepository.save(clock6);
            clockRepository.save(clock7);



            LocalDate localDate1=LocalDate.of(2022,6,8);
            LocalDate localDate2=LocalDate.of(2022,6,9);
            LocalDate localDate3=LocalDate.of(2022,6,8);

            Queue queue = new Queue(saveDoctor, localDate1, saveSick1, clock1000);
            Queue queue1 = new Queue(saveDoctor1, localDate2, saveSick2, clock2000);
            Queue queue2 = new Queue(saveDoctor2, localDate3, saveSick3, clock1000);

            queueRepository.save(queue);
            queueRepository.save(queue1);
            queueRepository.save(queue2);




        }
    }
}
